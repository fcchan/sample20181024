using System;
using System.Collections.Generic;
using System.Linq;
using Aia.Li.Db.Context.Agency;
using Aia.Li.Db.Context.Agency.Entities;
using Aia.Li.Db.Context.Agency.Providers;
using Aia.Li.Shared.ViewModels;
using Newtonsoft.Json;
using Aia.Li.Services.Data;

namespace Aia.Li.Services.DataService
{
    public class UserService : BaseService<UserModel, User>, IUserService
    {
        private readonly IUserProvider _users;
        private AuditTrailModelv2 audit;
        private readonly List<string> _managerScreens = new List<string> { "iposassignment", "iposacceptance" };
        private readonly List<string> _managerTypes = new List<string> { "dd", "um" };

        public UserService(IAgencyContext context) : base(context)
        {
            _users = _context.Users;
        }

        public UserService(IAgencyContext context, AuditTrailModelv2 currentTrail) : base(context)
        {
            _users = _context.Users;
            audit = currentTrail;
        }

        public IQueryable<User> All()
        {
            return _users.GetAll();
        }

        public User GetBy(int id)
        {
            return id == 0 ? null : _users.GetBy(id);
        }

        public User GetBy(string loginId)
        {
            return string.IsNullOrEmpty(loginId) ? null : _users.GetBy(loginId);
        }

        public UserModel GetModelBy(string loginId)
        {
            var user = GetBy(loginId);

            return new UserModel
            {
                Id = user.Id,
                LoginId = user.LoginId,
                IsAccountAdmin = user.IsAccountAdmin
            };

            //return _mapper.Map<UserModel>(user);
        }


        public User Create(UserModel vm)
        {
            var user = _mapper.Map<User>(vm);
            user.Password = string.Empty;
            _users.Create(user);
            _context.SaveChanges();

            _context.UserGroups.Create(new UserGroup { GroupId = 1, UserId = user.Id });
            _context.SaveChanges();

            return user;
        }


        /// <summary>
        /// Post authentication process. Handles Login counter and account locking. Checks expiry.
        /// </summary>
        /// <param name="user">User object containing user details</param>
        /// <param name="isValid">Authentication success / failure flag</param>
        /// <returns></returns>
        public KeyValuePair<int, string> ProcessLogin(User user, bool isValid)
        {
            if (user == null) return new KeyValuePair<int, string>(-1, "The username and password entered do no match.");

            if (user.Status == 2) return new KeyValuePair<int, string>(-3, "Your account is locked!. Please unlock your account.");

            if (!isValid)
            {
                if (user.LoginCounter == 2) user.Status = 2;
                user.LoginCounter += 1;
            }
            else
            {
                if (!user.LastChangedPassOn.HasValue || (DateTime.Now - user.LastChangedPassOn.Value).Days > 90) return new KeyValuePair<int, string>(2, "Account expired, Please change your password");
                user.LoginCounter = 0;
                user.Status = 1;
                user.PreviousLogin = user.LastLogin;
                user.LastLogin = DateTime.Now;

            }

            var updateResult = Update(user);

            return user.Status == 2
                ? new KeyValuePair<int, string>(-2, "You have reached your maximum retries, account is locked!.Please unlock your account.")
                : new KeyValuePair<int, string>(updateResult == 0 ? 0 : 1, updateResult == 0 ? "No records were" : $"{updateResult} records where affected.")
            ;



        }

        public int Update(UserModel vModel)
        {
            var user = _users.GetBy(vModel.Id);
            user.LoginId = vModel.LoginId;
            user.Password = vModel.Password;

            return Update(user);

        }


        public int UpdateAndAuditByLoginID(UserModel vModel)
        {
            var user = _users.GetBy(vModel.LoginId);
            //user.LoginId = vModel.LoginId;
            //user.Password = vModel.Password;

            audit.Adt_TargetTableName = "portal_user";
            audit.Adt_TargetTableRowID = user.Id.ToString();

            Dictionary<string, string> difference_oldvalue;
            Dictionary<string, string> difference_newvalue;
            MapAndDifferentiate(user, vModel, out difference_oldvalue, out difference_newvalue);

            audit.Adt_OldValue = JsonConvert.SerializeObject(difference_oldvalue, Formatting.Indented);
            audit.Adt_NewValue = JsonConvert.SerializeObject(difference_newvalue, Formatting.Indented);

            var updateResult = Update(user);

            audit.Adt_CreationTimeStamp = user.ModifiedDate;

            return updateResult;
        }


        private bool MapAndDifferentiate(User m, UserModel vm,
        out Dictionary<string, string> difference_oldvalue, out Dictionary<string, string> difference_newvalue)
        {
            difference_oldvalue = new Dictionary<string, string>();
            difference_newvalue = new Dictionary<string, string>();
            bool modified = false;

            if ((vm.Homepage_ID != null) && m.Homepage_ID.Equals(vm.Homepage_ID) == false)
            {
                difference_oldvalue.Add("Homepage_ID", m.Homepage_ID.ToString()); difference_newvalue.Add("Homepage_ID", vm.Homepage_ID.ToString());
                m.Homepage_ID = vm.Homepage_ID;
                modified = true;
            }

            if ((vm.ModifiedBy != 0) && m.ModifiedBy.Equals(vm.ModifiedBy) == false)
            {
                difference_oldvalue.Add(("ModifiedBy"), m.ModifiedBy.ToString()); difference_newvalue.Add("ModifiedBy", vm.ModifiedBy.ToString());
                m.ModifiedBy = vm.ModifiedBy;
                modified = true;
            }

            if (modified)
            {
                vm.ModifiedDate = DateTime.Now;
                difference_oldvalue.Add("ModifiedDate", m.ModifiedDate.ToString()); difference_newvalue.Add("ModifiedDate", vm.ModifiedDate.ToString());
                m.ModifiedDate = vm.ModifiedDate;
            }

            return modified;
        }


        /// <summary>Specifically derive the list of available pages for user 
        /// Written by fcchan 7 November 2017
        /// </summary>
        public List<PageModel> GetListOfHomepages(UserModel user, AgentService _agentService)
        {
            var _pageService = new PageService(_context);
            var _grpPageService = new GroupPageService(_context);
            //var _agentService = new AgentService(PortalContext);
            var _userGroupService = new UserGroupService(_context);

            var userGroups = _userGroupService.AllByUser(user.Id).Select(u => u.GroupId);
            var groupPagesViewable = from grpP in _grpPageService.All()
                                     join grpU in _userGroupService.AllByUser(user.Id) on grpP.GroupId equals grpU.GroupId
                                     where grpP.View == 1
                                     select new
                                     {
                                         user.Id,
                                         grpP.GroupId,
                                         grpP.FunctionIdentifier,
                                     };


            //LINQ expression cannot contain references to queries that are associated with different contexts.
            //agentInfo must be retrieved to a list first
            //assigning user row id to ID instead of agent row ID
            var agentInfo = (from agn in _agentService.All()
                             where agn.Number == user.LoginId
                             select new AgentModel
                             {
                                 ID = user.Id,
                                 Number = agn.Number,
                                 Type = agn.Type,
                             }).ToList<AgentModel>();


            var generalPages = from page in _pageService.All()
                               join grpP in groupPagesViewable on page.FunctionIdentifier equals grpP.FunctionIdentifier
                               where !_managerScreens.Contains(page.Controller.ToLower())
                               select new PageModel
                               {
                                   Id = page.Id,
                                   Title = page.Title,
                                   ListOrder = page.ListOrder,
                                   Controller = page.Controller,
                                   Action = page.Action
                               };

            //var managerPages = from page in _pageService.All()
            //                   join grpP in groupPagesViewable on page.FunctionIdentifier equals grpP.FunctionIdentifier
            //                   join agnt in agentInfo on grpP.UserId equals agnt.ID
            //                   where _managerScreens.Contains(page.Controller) && _managerTypes.Contains(agnt.Type.ToLower())
            //                   select new
            //                   {
            //                       page.Id,
            //                       page.Title,
            //                       page.ListOrder,
            //                       Controller = page.Controller,
            //                       Action = page.Action
            //                   };

            var managerPages = from agnt in agentInfo
                               join grpP in groupPagesViewable on agnt.ID equals grpP.Id
                               join page in _pageService.All() on grpP.FunctionIdentifier equals page.FunctionIdentifier
                               where _managerScreens.Contains(page.Controller.ToLower()) && _managerTypes.Contains(agnt.Type.ToLower())
                               select new PageModel
                               {
                                   Id = page.Id,
                                   Title = page.Title,
                                   ListOrder = page.ListOrder,
                                   Controller = page.Controller,
                                   Action = page.Action
                               };


            //there is internal error: Unable to process the type 'Anonymous type', because it has no known mapping to the value layer.
            //var allPages = generalPages.Union(managerPages).OrderBy(un => un.ListOrder);
            //therefore, to process the union separately instead of above code

            var ListofHomePages = new List<PageModel>();
            ListofHomePages.AddRange(generalPages.ToList());
            ListofHomePages.AddRange(managerPages.ToList());
            ListofHomePages.OrderBy(p => p.ListOrder);

            return ListofHomePages;
        }



        public int UpdatePasswordReset(int id, string tempPassword, string resetUnlock = "1")
        {
            var user = _users.GetBy(id);
            user.ResetUnlock = resetUnlock;
            user.LastChangedPassOn = DateTime.Now;
            user.TemporaryPassword = tempPassword;
            user.LoginCounter = 0;
            user.Status = 1;
            return Update(user);

        }

        public int Update(User user)
        {
            _users.Update(user);
            return _context.SaveChanges();
        }

        public int Delete(int id)
        {
            var user = _users.GetBy(id);
            _users.Delete(user);
            return _context.SaveChanges();

        }

    }
}
