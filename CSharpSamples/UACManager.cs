using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.WebPages.Html;
using Aia.Li.Db.Context.Agency.Entities;
using Aia.Li.Services.Data;
using Aia.Li.Services.DataService;
using Aia.Li.Services.Utilities;
using Aia.Li.Shared.ViewModels;
using App.Enums;
using App.Models;
using App.Providers;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.OAuth;

namespace App.Managers
{
    public class UacManager : BaseManager
    {

        private readonly UserService _userSvc;
        private readonly AdminUserService _adminUserSvc;
        private readonly SecurityService _security;

        public UacManager(string clientType): base(clientType)
        {
            _userSvc = new UserService(AgencyContext, AuditTrailModelProvider.CurrentAudit);
            _adminUserSvc = new AdminUserService(AgencyContext);
            _security = new SecurityService();

        }

        public KeyValuePair<byte, string> Login(OAuthGrantResourceOwnerCredentialsContext context, MetaDataProvider provider, bool isValid = true)
        {
            var user = _userSvc.GetBy(context.UserName);

            var updateStatus = _userSvc.ProcessLogin(user, isValid);

            if (new [] {-1, -2, -3}.Contains(updateStatus.Key)) return new KeyValuePair<byte, string>(0, updateStatus.Value);

            var logUserSvc = new LogUserService(AgencyContext);
            var intruder = isValid ? context.UserName : $"user_id={context.UserName}, password={context.Password}";
            var logUser = logUserSvc.Make(intruder, provider.MetaModel);
            logUserSvc.Create(logUser);

            return new KeyValuePair<byte, string>(1, string.Empty);

        }

        public ResponseMessage ChangePassword(ApplicationUserManager userManager, string userId, string username, ChangePasswordBindingModel model, Dictionary<string, string> metaInfo)
        {

            var user = _userSvc.GetBy(username);
            if (user == null) return ResponseBuilder.BuildBadRequest(new [] {"Unable to perform password change!!"});

            var securityModel = GetSecurityModel(username, model);
            var validateResult = _security.ValidatePassword(securityModel);
            if (!validateResult.Key) return ResponseBuilder.BuildBadRequest(new [] {validateResult.Value});

            var pwdBannedWordService = new PasswordBannedWordService(PortalContext);
            if (pwdBannedWordService.IsBanned(securityModel.Password) == true) return ResponseBuilder.BuildBadRequest(new[] { "Password cannot be dictionary word or common sequence like abcd, 1234 etc. " });

            var pwdHistoryService = new PasswordHistoryService(AgencyContext);
            var passwords = pwdHistoryService.GetAll(user.Id)?.OrderByDescending(o => o.CreateDate).Take(8).Select(s => s.Password).ToList();

            if (passwords == null || passwords.Contains(model.NewPassword)) return ResponseBuilder.BuildBadRequest(new[] { "Recently used password is not allowed!! " });

            var result = userManager.ChangePassword(userId, model.OldPassword, model.NewPassword);

            if (!result.Succeeded) return ResponseBuilder.BuildBadRequest(result.Errors?.ToArray());

            _userSvc.UpdatePasswordReset(user.Id, string.Empty, "0");

            pwdHistoryService.Create(pwdHistoryService.Map(user.Id, model.NewPassword , user.Id));

            AuditService.CreateAuditTrail(metaInfo["ipAddress"], "Insert", user.LoginId, "portal_user", 0, "", "");


        return ResponseBuilder.BuildSuccess("Password changed successfully");
        }


        /// <summary>Loads pages that are authorized for user. 
        /// The function loads same pages from UiManager.Navigation(int userId)
        /// Written by fcchan 6 November 2017
        /// <seealso cref="UiManager.cs"/>
        /// </summary>
        public ResponseMessage LoadHomepages(UserModel user)
        {

            if (user != null)
            {
                var homepage = from usr in _userSvc.All()
                               where usr.Id == user.Id
                               select new
                               {
                                   usr.Homepage_ID,
                               };

                var _agentService = new AgentService(PortalContext);
                var ListofHomePages = _userSvc.GetListOfHomepages(user, _agentService);
                ListofHomePages.Insert(0, new PageModel { Id = 0, Title = "--- System Default ---", ListOrder = 0, Controller = "", Action = "" });
                MultiResultSetModel multiResultSet = new MultiResultSetModel
                {
                    Result1 = ListofHomePages.Cast<object>().ToList(),
                    Result2 = homepage.ToList<object>(),
                    Result3 = null
                };

                return ResponseBuilder.BuildData(HttpStatusCode.OK, null, null, multiResultSet);
            }
            else
            {
                return ResponseBuilder.BuildBadRequest(new[] { "Invalid user because it is not a valid value" });
            }
        }


        /// <summary>Save homepage that was authorized for user. 
        /// Written by fcchan 6 November 2017
        /// </summary>
        public ResponseMessage SaveHomepage(UserModel user, PageModel page)
        {
            var _agentService = new AgentService(PortalContext);
            var ListofHomePages = _userSvc.GetListOfHomepages(user, _agentService);
            ListofHomePages.Add(new PageModel { Id = 0, Title = "Allow System Default", ListOrder = 0, Controller = "", Action = "" }); //to allow System Default as valid criteria

            if (page.Id < 0 || !ListofHomePages.Any(h => h.Id == page.Id))
            {
                return ResponseBuilder.BuildBadRequest(new[] { "The homepage is no longer valid. Please refresh this page again." });
            }

            user.Homepage_ID = page.Id;
            user.ModifiedBy = user.Id;
            _userSvc.UpdateAndAuditByLoginID(user);
            AuditService.CreateAuditTrail(AuditTrailModelProvider.CurrentAudit);
            return ResponseBuilder.BuildSuccess($"Homepage is updated successfuly!");
        }


        public ResponseMessage PageList()
        {
            var service = new PageService(AgencyContext);
            var pages = service.All();

            return ResponseBuilder.BuildData(HttpStatusCode.OK, null, null, pages.ToArray());
        }





        //public IEnumerable<UserModel> GetUsers()
        //{
        //    var users = _userSvc.All();
        //    return users.Project().To<UserModel>();
        //}

        public ResponseMessage GetPermissions(int id)
        {
            var permissions = Permissions(id);

            return ResponseBuilder.BuildData(HttpStatusCode.OK, null, null, permissions);
        }


        public IEnumerable<GroupPage> Permissions(int id)
        {
            var grpPageSvc = new GroupPageService(AgencyContext);
            var permissions = grpPageSvc.AllByGroups(id);

            return permissions;

        }

        public bool CanView(int userId, int pageId)
        {
            return HasAccess(userId, pageId, AccessType.ViewOnly);
        }

        public bool CanAdd(int userId, int pageId)
        {
            return HasAccess(userId, pageId, AccessType.Add);
        }

        public bool CanUpdate(int userId, int pageId)
        {
            return HasAccess(userId, pageId, AccessType.Update);
        }

        public bool HasAccess(int userId, int pageId, AccessType accessType)
        {
            var pages = GetUserPageAccess(userId, pageId);

            if (pages == null) return false;

            switch (accessType)
            {
                case AccessType.ViewOnly:
                    return pages.Any(p => p.View == 1);
                case AccessType.Add:
                    return pages.Any(p => p.Add == 1);
                case AccessType.Update:
                    return pages.Any(p => p.Update == 1);
                default:
                    return false;

            }
        }

        public ResponseMessage PagePermissions(int userId, int pageId)
        {
            var pages = GetUserPageAccess(userId, pageId);

            if (pages == null) return null;

            var groupPages = pages as GroupPage[] ?? pages.ToArray();

            var returnData = new Dictionary<string, bool>()
            {
                {"view",  groupPages.Any(p => p.View == 1)},
                {"add",  groupPages.Any(p => p.Add == 1)},
                {"update",  groupPages.Any(p => p.Update == 1)},

            };
            return ResponseBuilder.BuildData(HttpStatusCode.OK, new[] { "Data available" }, null, returnData);

        }

        private IEnumerable<GroupPage> GetUserPageAccess(int userId, int pageId)
        {
            var permissions = Permissions(userId);
            return permissions?.Where(p => p.FunctionIdentifier == pageId.ToString());
        }


        public int GetPageId(string pageUrl)
        {
            var page = new PageService(AgencyContext).All().FirstOrDefault(p => p.Url.Trim().ToLower() == pageUrl.Trim().ToLower());
            return page?.Id ?? 0;
        }
    }
}